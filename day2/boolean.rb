isOnline=true
isAdult=false
puts "Your Status Online :#{isOnline}\nYour Age Eligibilty :#{false}"
=begin
Booleans

If you try to evaluate a value other than true or false as a Boolean, Ruby will automatically treat it as a 
Boolean. 
When this is done, a non-Boolean value that evaluates to true is called "truthy" and a non-Boolean value that 
evaluates to false is called "falsey".

//IMPORTANT:// In Ruby only false and nil are falsey. Everything else is truthy (even 0 is truthy).

=end
