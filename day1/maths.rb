#Mathematical Operations
#area of circle
Pi=3.14
r=3
puts "area of circle : #{Pi*r*r} "

=begin
When you divide two integer values, the result will be an integer, as shown in the above example.
 If you want to have a floating point result, one operand must be a floating point value:
=end
x = 5.0
y = 2
puts "example for float mix int : #{x/y}" # outputs 2.5

#sum of marks
s1=34
s2=45
s3=23
puts "total marks : #{s1+s2+s3} "

#avreage of marks
puts "average of marks : #{(s1+s2+s3)/3}"

#difference
a=1
b=8
puts "difference of #{b} and #{a} is #{b-a}"

#modulus operator
puts "modulus of #{a} and #{b} is #{a%b}"

#Exponent

=begin
The ** represents the exponent operator
for raising a number to a power to perform exponentiation.
=end

a=5
b=2

puts "exponent of #{a} and #{b} is #{a**b}"

#self assignment operator
x=1
y=1
puts "x =#{x}  y=#{y}"
x += y  # x=x+y
puts "x =#{x}  y=#{y}"
x -= y  # x=x-y
puts "x =#{x}  y=#{y}"
x *= y  # x=x*y
puts "x =#{x}  y=#{y}"
x /= y  # x=x/y
puts "x =#{x}  y=#{y}"
x %= y  # x=x%y
puts "x =#{x}  y=#{y}"
x **= y  # x=x**y
puts "x =#{x}  y=#{y}"
