# Welcome to Ruby!
###### :point_right: It is ranked among the top 10 programming languages worldwide. Much of its growth is attributed to the popularity of software written in Ruby, particularly the **Ruby on Rails web** framework.

> A quote from its creator, Yukihiro "Matz" Matsumoto: "Ruby is simple in appearance, but is very complex inside, just like our human body."
Matsumoto has said that Ruby is designed for programmer productivity and fun, following the principles of good user interface design.

 
> Ruby is also completely free. Not only free of charge, but also free to use, copy, modify, and distribute.

 ###### :point_right: Ruby is a dynamic, object-oriented, general-purpose programming language.

 ###### :point_right: In Ruby, everything (even a simple number) is an object.

